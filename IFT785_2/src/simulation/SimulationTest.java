/**
 * 
 */
package simulation;

import java.util.HashMap;

import beans.Beans;
import beans.Sensor;
import junit.framework.TestCase;

public class SimulationTest extends TestCase {

	private Simulation simulation;
	
	private Sensor agent;
	private Sensor sensor;
	private Sensor effector;
	private HashMap<Beans,Beans> linkSensorEffector;
	/* (non-Javadoc)
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
		
		agent = new Sensor (0,0);
		
		
		
		simulation = Simulation.getInstance();
		simulation.setAgent(agent);
		
		sensor = new Sensor(0,1);
		effector = new Sensor(0,2);

		HashMap<Beans,Beans> map = new HashMap<Beans,Beans> ();
		map.put(sensor, effector);
		
		simulation.addSensor(sensor);
		simulation.addEffector(effector);
		simulation.setLinkSensorEffector(map);
		
		
	}

	/* (non-Javadoc)
	 * @see junit.framework.TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
		
		simulation = null;
	}

	/**
	 * Test method for {@link simulation.Simulation#getAgent()}.
	 */
	public void testGetAgent() {
		try {
			simulation.getAgent();
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	/**
	 * Test method for {@link simulation.Simulation#updateAgent()}.
	 */
	public void testUpdateAgent() {
		try {
			simulation.updateAgent(new int[]{1,1});
		} catch (Exception e) {
			System.out.println(e);
		}

		assertEquals(simulation.getAgent().getX(), 1);
		assertEquals(simulation.getAgent().getY(), 1);
	}
	/**
	 * Test method for {@link simulation.Simulation#getInstance()}.
	 */
	public void testGetInstance() {
		assertEquals(simulation, Simulation.getInstance());
		assertTrue(!simulation.equals(null));
		
	}
	/**
	 * Test method for {@link simulation.Simulation#setAgen(tbeans.Beans)}.
	 */
	public void testSetAgent() {
		try {
			simulation.setAgent(agent);
		} catch (Exception e) {
			System.out.println("Could not set agent");
		}
	}

	/**
	 * Test method for {@link simulation.Simulation#notifyObjectConnected()}.
	 */
	public void testNotifyObjectConnected() {
		try {
			simulation.notifyObjectConnected();
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	/**
	 * Test method for {@link simulation.Simulation#getLinkSensorEffector()}.
	 */
	public void testGetMap() {
		try {
			simulation.getLinkSensorEffector();
		} catch (Exception e) {
			System.out.println(e);
		}	}

	/**
	 * Test method for {@link simulation.Simulation#setLinkSensorEffector(java.util.HashMap)}.
	 */
	public void testSetLinkSensorEffector() {
		assertTrue(simulation.getListSensor().contains(sensor));
		assertTrue(simulation.getListEffector().contains(effector));
		
		try {
			simulation.setLinkSensorEffector(linkSensorEffector);
		} catch (Exception e) {
			System.out.println("Could not set map");
		}
	}

	/**
	 * Test method for {@link simulation.Simulation#getListSensor()}.
	 */
	public void testGetListSensor() {
		try {
			simulation.getListSensor();
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	/**
	 * Test method for {@link simulation.Simulation#getListEffector()}.
	 */
	public void testGetListEffector() {
		try {
			simulation.getListEffector();
		} catch (Exception e) {
			System.out.println(e);
		}
	}


	/**
	 * Test method for {@link simulation.Simulation#addSensor(beans.Beans)}.
	 */
	public void testAddSensor() {
		try {
			simulation.addSensor(sensor);
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	/**
	 * Test method for {@link simulation.Simulation#addEffector(beans.Beans)}.
	 */
	public void testAddEffector() {
		try {
			simulation.addEffector(effector);
		} catch (Exception e) {
			System.out.println("Could not set effector");
		}
	}

}


