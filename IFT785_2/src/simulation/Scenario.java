/**
 * 
 */
package simulation;

import java.util.Vector;

/**
 * @author ahmed
 *
 */
public class Scenario {
	int simulationState;
	Vector<int[]> movement;
	Simulation simulation;
	private boolean pause;
	
	/**
	 * @param simulation the simulation to set
	 */
	public void setSimulation(Simulation simulation) {
		this.simulation = simulation;
	}
	/**
	 * @return the simulationState
	 */
	public int getSimulationState() {
		return simulationState;
	}
	/**
	 * @param simulationState the simulationState to set
	 */
	public void setSimulationState(int simulationState) {
		this.simulationState = simulationState;
	}
	/**
	 * @return the movement of the agent
	 */
	public Vector<int[]> getMovement() {
		return movement;
	}
	/**
	 * @param movement the movement to set
	 */
	public void setMovement(Vector<int[]> movement) {
		this.movement = movement;
	}
	/**
	 * @param movement
	 */
	public Scenario(Vector<int[]> movement) {
		super();
		setMovement( movement);
		setSimulationState(0);
	}
	/**
	 * Pause the simulation
	 */
	void clickPause() {
		pause = true;
	}
	/**
	 * Start and resume the simulation depending of the simulation state
	 */
	public void run() {
		pause = false;
		
		for (int i = getSimulationState(); i < getMovement().size(); i++) {
			if (pause){
				setSimulationState(i);
				break;
			}
			simulation.updateAgent(getMovement().elementAt(i));

			simulation.notifyObjectConnected();
		}
	}
}
