package simulation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import beans.*;

public class Simulation {
	List<Beans> listSensor;
	List<Beans> listEffector;
	Beans agent;
	Scenario scenario;
	HashMap<Beans,Beans> linkSensorEffector;

	private static volatile Simulation instance = null;
	
	/*
	 * GETTERS & SETTERS
	 */
	
	/**
	 * @return the scenario
	 */
	public Scenario getScenario() {
		return scenario;
	}
	/**
	 * @param scenario the scenario to set
	 */
	public void setScenario(Scenario scenario) {
		this.scenario = scenario;
		scenario.setSimulation(this);
	}
	/**
	 * @return the listSensor
	 */
	public List<Beans> getListSensor() {
		return listSensor;
	}
	/**
	 * @param listSensor the listSensor to set
	 */
	public void setListSensor(List<Beans> listSensor) {
		this.listSensor = listSensor;
	}
	/**
	 * @return the listEffector
	 */
	public List<Beans> getListEffector() {
		return listEffector;
	}
	/**
	 * @param listEffector the listEffector to set
	 */
	public void setListEffector(List<Beans> listEffector) {
		this.listEffector = listEffector;
	}
	
	/**
	 * @return the linkSensorEffector
	 */
	public HashMap<Beans, Beans> getLinkSensorEffector() {
		return linkSensorEffector;
	}
	/**
	 * @param linkSensorEffector the linkSensorEffector to set
	 */
	public void setLinkSensorEffector(HashMap<Beans, Beans> linkSensorEffector) {
		this.linkSensorEffector = linkSensorEffector;
	}
	/**
	 * @return the agent 
	 */
	public Beans getAgent() {
		return agent;
	}
	/**
	 * @param agent the agent to set
	 */
	public void setAgent(Beans agent) {
		this.agent = agent;
	}
	
	/**
	 * Simulation constructor
	 * @param agent
	 * @param movement
	 */
	 private  Simulation() {
		super();
		
		listEffector = new ArrayList<Beans>();
		listSensor = new ArrayList<Beans>();
		
	}
	 
	
	/*
	 * METHODS
	 */
	 /**
		 * Implémente le patron Singleton pour instancier la Simulation une seule fois
		 * @param sensor
		 */
	 public final static Simulation getInstance() {
		 if (Simulation.instance == null) {
	            
	            synchronized(Simulation.class) {
	              if (Simulation.instance == null) {
	                Simulation.instance = new Simulation();
	              }
	            }
	         }
	         return Simulation.instance;
		 
	 }
	/**
	 * Add sensor for Observer Pattern
	 * @param sensor
	 */
	public void addSensor(Beans sensor) {
		listSensor.add(sensor);
	}
	
	/**
	 * Add effector for Observer Pattern
	 * @param effector
	 */
	public void addEffector(Beans effector) {
		listEffector.add(effector);
	}
	

	
	
	/**
	 * 
	 */
	public void notifyObjectConnected() {
		for (Beans sensor  : listSensor) {

			if(sensor.action(agent)) {
				linkSensorEffector.get(sensor).action();
			}
		}
	}
	public void updateAgent(int[] elementAt) {
		agent.setPosition(elementAt);
		System.out.println("Position agent : " + agent.getX() + agent.getY());
		
		
	}
	
	
}
