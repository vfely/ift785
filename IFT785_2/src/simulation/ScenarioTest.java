/**
 * 
 */
package simulation;

import java.util.Vector;

import beans.Sensor;
import junit.framework.TestCase;

/**
 * @author ahmed
 *
 */
public class ScenarioTest extends TestCase {
	private Vector<int []> movement;
	private Simulation simulation;
	private Scenario scenario;
	/* (non-Javadoc)
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
		movement = new Vector<int[]>();
		movement.add(new int[]{0,0});
		movement.add(new int[]{0,1});
		movement.add(new int[]{0,2});
		scenario=new Scenario(movement);
		simulation=Simulation.getInstance();
		simulation.setAgent(new Sensor(0, 0));
		scenario.setSimulation(simulation);
		
	}
	
	/**
	 * Test method for {@link simulation.Scenario#setMovement(java.util.Vector)}.
	 */
	public void testSetMovement() {
		try {
			scenario.setMovement(movement);
		} catch (Exception e) {
			System.out.println("Could not set deplacement");
		}
	}
	public void testSetSimulation() {
		try {
			scenario.setSimulation(simulation);
		} catch (Exception e) {
			System.out.println("Could not set deplacement");
		}
	}
	public void testScenario() {
		assertTrue(!scenario.equals(null));
		  assertTrue(!scenario.getMovement().equals(null));
	}
	public void testSetSimulationState() {
		try {
			scenario.setSimulationState(1);
			
		} catch (Exception e) {
			System.out.println("Could not set deplacement");
		}
		assertEquals(scenario.getSimulationState(), 1);
	}
	/**
	 * Test method for {@link simulation.Scenario#getSimulationState()}.
	 */
	public void testGetSimulationState() {
		try {
			scenario.getSimulationState();
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	/**
	 * Test method for {@link simulation.Simulation#run()}.
	 */
	public void testRun() {
		try {
			scenario.run();
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	/* (non-Javadoc)
	 * @see junit.framework.TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}

}

