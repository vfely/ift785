package beans;

public class Sensor extends Beans {
	int x;
	int y;
	
	public Sensor (int x, int y) {
		this.x = x;
		this.y = y;

	}
	
	@Override
	public void afficher() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setPosition(int[] elementAt) {
		setX(elementAt[0]);
		setY(elementAt[1]);
	}

	@Override
	public Boolean action(Beans agent) {

		if ((agent.getX() == x) && (agent.getY() == y)) {
			System.out.println("Action déclanchée");
			return true;
		}
		else	
			return false;
	}

	@Override
	public void action() {
		System.out.println("Lampe allumée");
	}

}
